"""
File: genga_visu.py

This routine generates animated plots to visualize the outputs of the Genga simulations. 
As a first step the mass M will be displayed as a function of semi-major axis a.
Secondly the eccentricity is displayed as a function of semi-major axis (with the size of the dot
                                                                         proportional to the mass)

Author: Sarah Joiret 

Version   Date            Editor          Comment
-------   ----------   --------------   ----------------------------------------------------------
0.1       17 May 2022   Sarah Joiret     First tests. Not running.
0.2       14 Jun 2022   Sarah Joiret     Update. Running. /!\ The images of the giant planets are 
                                         not displayed. 

"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
import matplotlib.image as mpimg
import glob
plt.rcParams['animation.ffmpeg_path'] = '/opt/homebrew/bin/ffmpeg'
from astropy.constants import G, M_sun, au 

def cart_2_kep(x, y, z, vx, vy, vz):
    """
    Converts cartesian coordinates to Keplerian coordinates

    Parameters
    ----------
    x : float
        position (x-axis)
    y : float
        position (y-axis)
    z : float
        position (z-axis)
    vx : float
        velocity (x-axis) in AU/day' (must be multiplied by 0.01720209895 to be converted in AU/day)
    vy : float
        velocity (y-axis) in AU/day' (must be multiplied by 0.01720209895 to be converted in AU/day)
    vz : float
        velocity (z-axis) in AU/day' (must be multiplied by 0.01720209895 to be converted in AU/day)

    Returns
    -------
    a, e: float
        Keplerian coordinates of the protoplanets / planetesimals 

    """
    # Convert the units of the velocity from AU/day' to AU/day
    vx = vx * 0.01720209895
    vy = vy * 0.01720209895
    vz = vz * 0.01720209895
    
    # Compute the radius, r, and velocity v
    r = np.sqrt(x**2 + y**2 + z**2)
    v = np.sqrt(vx**2 + vy**2 + vz**2)
    
    # Compute vector r and vector v
    r_vector = np.array([x, y, z])
    v_vector = np.array([vx, vy, vz])
    
    # Compute the specific angular momentum
    h_vector = np.cross(r_vector, v_vector)
    h = np.sqrt(h_vector[0]**2 + h_vector[1]**2 + h_vector[2]**2)
    
    # Compute the Standard gravitational parameter in AU/day
    mu_ms = G.value * M_sun.value # Standard gravitational parameter = GM of the central body
                                  # with G the Newtonian constant of gravitation (in m^3s^-2) 
    mu = mu_ms * (86400**2) / ((au.value)**3)    # There are 86400 s in a Julian day 
    
    # Compute the specific energy E
    E = ((v**2) / 2) - (mu / r)
    
    # Compute semi-major axis a
    a = - mu / (2 * E)
    
    # Compute eccentricity e
    e = np.sqrt(1 - (h**2 / (a * mu)))
    
    return a, e


class body:
    time:np.ndarray
    a:np.ndarray
    e:np.ndarray
    x:np.ndarray
    y:np.ndarray
    z:np.ndarray
    vx:np.ndarray
    vy:np.ndarray
    vz:np.ndarray
    mass:np.ndarray
    
    def __init__(self, filename: str):
        # Load the data from the Out*.dat file
        data = np.loadtxt(filename)
        # If only one line in the Out*.dat, the data are not a 2-D list and the values
        # must be loaded differently
        if data.shape == (21,):
            self.time = np.asarray([data[0]])
            self.mass = np.asarray([data[2]])
            self.x = np.asarray([data[4]])
            self.y = np.asarray([data[5]])
            self.z = np.asarray([data[6]])
            self.vx = np.asarray([data[7]])
            self.vy = np.asarray([data[8]])
            self.vz = np.asarray([data[9]])
            
        else:
            self.time = data[:,0]
            self.mass = data[:,2]
            self.x = data[:,4]
            self.y = data[:,5]
            self.z = data[:,6]
            self.vx = data[:,7]
            self.vy = data[:,8]
            self.vz = data[:,9]

        self.len_time = len(self.time)
        self.a = np.zeros(self.len_time)
        self.e = np.zeros(self.len_time)
        for i in range(self.len_time):
            self.a[i] = cart_2_kep(self.x[i], self.y[i], self.z[i], self.vx[i], self.vy[i], self.vz[i])[0]
            self.e[i] = cart_2_kep(self.x[i], self.y[i], self.z[i], self.vx[i], self.vy[i], self.vz[i])[1]
        
            
# Initialize the body list
body_list = []

# Find the directory in which the data are stored and fetch all files from the directory
for filename in glob.glob('/Users/sarahjoiret/Desktop/sims/genga/case2_output/big/clement/10000comets/1/Outclement_p000*.dat', recursive=True):
    print(filename)
    body_list.append(body(filename))

# -----------------------------------------------------------------------------------------------
# FIGURE 1: temporal evolution of the mass as a function of the semi major-axis
# -----------------------------------------------------------------------------------------------

# Create the figure
fig, ax = plt.subplots()
scatter = ax.scatter([body.a[0] for body in body_list],[body.mass[0] for body in body_list], c= 'black', marker='.')
ax.set_xlabel('semi-major axis a/AU', fontsize=12)
ax.set_ylabel('mass M/$M_\u2609$', fontsize=12)
ax.set_title('Time = 0 Myrs')
ax.set_xlim(0.3, 3)
ax.set_ylim(-0.1e-6, 3.2e-6)

# Define the interval of time for the animation
dt = 21902.80629705681 # in years

# Animation function
def animate(i):
    i = 1 * i
    
    # Clear the plot for each iteration
    ax.clear()
    
    # iteration of time
    t = i * dt
    
    # Insert images of the terrestrial  and giant planets
    mercury = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/mercury.png')
    venus = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/venus.png')
    earth = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/earth.png')
    mars = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/mars.png')
    jupiter = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/jupiter.png')
    saturn = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/saturn.png')
    uranus = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/uranus.png')
    neptune = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/neptune.png')

    im_mercury = OffsetImage(mercury, zoom=0.4) 
    im_venus = OffsetImage(venus, zoom=0.4)
    im_earth = OffsetImage(earth, zoom=0.4)
    im_mars = OffsetImage(mars, zoom=0.4)  
    im_jupiter = OffsetImage(jupiter, zoom=0.4)
    im_saturn = OffsetImage(saturn, zoom=0.4)
    im_uranus = OffsetImage(uranus, zoom=0.4)
    im_neptune = OffsetImage(neptune, zoom=0.4)

    ab_me = AnnotationBbox(im_mercury, (0.387, 1.651e-7), frameon=False)
    ab_ve = AnnotationBbox(im_venus, (0.723, 2.447e-6), frameon=False)
    ab_ea = AnnotationBbox(im_earth, (1.0, 3.003e-6), frameon=False)
    ab_ma = AnnotationBbox(im_mars, (1.524, 3.2e-7), frameon=False)
    ab_ju = AnnotationBbox(im_jupiter, (5.2029, 9.548e-4), frameon=False)
    ab_sa = AnnotationBbox(im_saturn, (9.537, 2.857e-4), frameon=False)
    ab_ur = AnnotationBbox(im_uranus, (19.189, 4.365e-5), frameon=False)
    ab_ne = AnnotationBbox(im_neptune, (30.07, 5.149e-5), frameon=False)
     
    ax.add_artist(ab_me)
    ax.add_artist(ab_ve)
    ax.add_artist(ab_ea)
    ax.add_artist(ab_ma)
    ax.add_artist(ab_ju)
    ax.add_artist(ab_sa)
    ax.add_artist(ab_ur)
    ax.add_artist(ab_ne)
    
    # For each body of the body list, take the semi-major axis of the corresponding 
    # iteration time
    a = [  body.a[i] if i < body.len_time else None   for body in body_list ]
    # For each body of the body list, take the mass of the corresponding iteration time
    M =  [  body.mass[i] if i < body.len_time else None   for body in body_list ]
    
    # Plotting the planetesimals 
    ax.set_xlabel('semi-major axis a/AU', fontsize=12)
    ax.set_ylabel('mass M/$M_\u2609$', fontsize=12)
    ax.set_xlim(0.3, 3)
    ax.set_ylim(-0.1e-6, 3.2e-6)
    ax.set_title('Time = ' + str(np.round(t/1000000, 3)) + ' Myrs')
    ax.scatter(a, M, c= 'black', marker='.')
    
    # Plot the terrestrial planet as dots (Mercury, Venus, Earth and Mars)
    #ax.scatter(0.387, 1.651e-7, color='saddlebrown', marker='D', label='Mercury')
    #ax.scatter(0.723, 2.447e-6, color='sandybrown', marker='D', label='Venus')
    #ax.scatter(1.0, 3.003e-6, color='royalblue', marker='D', label='Earth')
    #ax.scatter(1.524, 3.2e-7, color='firebrick', marker='D', label='Mars')
    #ax.legend()

# Call the animation
ani = FuncAnimation(fig, animate, frames=10001, interval=0.001, repeat=False)

# Show the animated plot
plt.show()

# Save the animated plot
#writer = PillowWriter(fps=1000)
#ani.save("terrestrial1.gif", writer=writer)

# -----------------------------------------------------------------------------------------------
# FIGURE 2: temporal evolution of the eccentricity as a function of the semi major-axis
# -----------------------------------------------------------------------------------------------
# Create the figure
fig2, ax2 = plt.subplots(figsize=(35, 8))
a_0 = [body.a[0] for body in body_list]
e_0 = [body.e[0] for body in body_list]
ax2.scatter(a_0, e_0, c= 'black', marker='.')
ax2.set_xlabel('semi-major axis a/AU', fontsize=12)
ax2.set_ylabel('eccentricity e', fontsize=12)
ax2.set_title('Time = 0 Myrs')
#ax2.set_xscale('log')
ax2.set_xlim(0.3, 6)
ax2.set_ylim(-0.05, 1.05)

# Define the interval of time for the animation
dt = 21902.80629705681 # in years

# Animation function
def animate2(j):
    
    # Clear the plot for each iteration
    ax2.clear()
    
    # iteration of time
    t = j * dt
    
    # Insert images of the terrestrial and giant planets
    mercury = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/mercury.png')
    venus = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/venus.png')
    earth = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/earth.png')
    mars = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/mars.png')
    jupiter = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/jupiter.png')
    saturn = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/saturn.png')
    uranus = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/uranus.png')
    neptune = mpimg.imread('/Users/sarahjoiret/Desktop/images/icons/neptune.png')

    im_mercury = OffsetImage(mercury, zoom=0.4*0.18) # should be *0.09 to be on scale 
    im_venus = OffsetImage(venus, zoom=0.4*1.37)
    im_earth = OffsetImage(earth, zoom=0.4*1.67)
    im_mars = OffsetImage(mars, zoom=0.4*0.36)  # should be *0.18 to be on scale 
    im_jupiter = OffsetImage(jupiter, zoom=0.4*2)
    im_saturn = OffsetImage(saturn, zoom=0.4*2)
    im_uranus = OffsetImage(uranus, zoom=0.4*2)
    im_neptune = OffsetImage(neptune, zoom=0.4*2)

    ab_me = AnnotationBbox(im_mercury, (0.387, 0.2056), frameon=False)
    ab_ve = AnnotationBbox(im_venus, (0.723, 0.0068), frameon=False)
    ab_ea = AnnotationBbox(im_earth, (1.0, 0.01671), frameon=False)
    ab_ma = AnnotationBbox(im_mars, (1.524, 0.0935), frameon=False)
    ab_ju = AnnotationBbox(im_jupiter, (5.2029, 0.0485), frameon=False)
    ab_sa = AnnotationBbox(im_saturn, (9.537, 0.0529), frameon=False)
    ab_ur = AnnotationBbox(im_uranus, (19.189, 0.0438), frameon=False)
    ab_ne = AnnotationBbox(im_neptune, (30.07, 0.0144), frameon=False)
     
    ax2.add_artist(ab_me)
    ax2.add_artist(ab_ve)
    ax2.add_artist(ab_ea)
    ax2.add_artist(ab_ma)
    ax2.add_artist(ab_ju)
    ax2.add_artist(ab_sa)
    ax2.add_artist(ab_ur)
    ax2.add_artist(ab_ne)
    
    # For each body of the body list, take the semi-major axis of the corresponding 
    # iteration time
    a_none = [  body.a[j] if j < body.len_time else None   for body in body_list ]
    # Remove the "None" values
    a = []
    for val_a in a_none:
        if val_a != None :
            a.append(val_a)

    # For each body of the body list, take the eccentricity of the corresponding iteration time
    e_none =  [  body.e[j] if j < body.len_time else None   for body in body_list ]
    # Remove the "None" values
    e = []
    for val_e in e_none:
        if val_e != None :
            e.append(val_e)
    
    # For each body of the body list, take the mass of the corresponding iteration time
    M_none =  [  body.mass[j] * 5e8 if j < body.len_time else None   for body in body_list ]
    # Remove the "None" values
    M = []
    for val_M in M_none:
        if val_M != None :
            M.append(val_M)

    # Plotting the planetesimals 
    ax2.set_xlabel('semi-major axis a/AU', fontsize=12)
    ax2.set_ylabel('eccentricity e', fontsize=12)
    #ax2.set_xscale('log')
    ax2.set_xlim(0.3, 6)
    ax2.set_ylim(-0.05, 1.05)
    ax2.set_title('Time = ' + str(np.round(t/1000000, 3)) + ' Myrs')
    ax2.scatter(a, e, c= 'black', s=M, marker='.')

# Call the animation
ani2 = FuncAnimation(fig2, animate2, frames=11, interval=100, repeat=True)

# Show the animated plot
plt.show()
