"""
File: initialdat_generator.py

This routine generates an initial.dat file that can be used as an input for the Genga simulations.
The orbital elements can either be in cartesian or keplerian coordinates. 

Author: Sarah Joiret

Version   Date            Editor          Comment
-------   ----------   --------------   ----------------------------------------------------------
0.1       07 Mar 2022   Sarah Joiret     First tests.
0.2       12 Apr 2022   Sarah Joiret     Added the outer planetesimal disk and an automatic way to 
                                         generate the 10 different folders for each specific 
                                         configuration
0.3       27 May 2022   Sarah Joiret     Adding the keplerian coordinates                          

"""

import numpy as np
import os
from astropy.constants import G, M_earth, M_sun, au 
from math import pi
import shutil

# =================================================================================================
# In this section a few auxiliary functions are defined which are needed for the routine
def mass_generator(nb_Earth, nb_pp, nb_pl):
    """
    Generates the mass of the terrestrial protoplanets (embryos) and planetesimals

    Parameters
    ----------
    nb_Earth : float
        Number of Earth masses for the inner disk
    nb_pp : integer
        Number of terrestrial protoplanets
    nb_pl : integer
        Number of inner planetesimals

    Returns
    -------
    M_pp: float
        Mass of a terrestrial protoplanets
    M_pl: float
        Mass of a planetesimal

    """
    M_earth_SU = M_earth.value / M_sun.value  # Earth's mass in solar units
    M_tot = nb_Earth * M_earth_SU  # Total mass of embryos + planetesimals in solar units
    
    M_pp_tot = 0.9 * M_tot # Total mass of embryos
    M_pp = M_pp_tot / nb_pp  # Mass of each planetary embryo
    
    M_pl_tot = 0.1 * M_tot # Total mass of planetesimals
    M_pl = M_pl_tot / nb_pl  # Mass of each planetesimal
    
    return M_pp, M_pl

def massc_generator(nbc_Earth, nb_co):
    """
    Generates the mass of the comets

    Parameters
    ----------
    nbc_Earth : float
        Number of Earth masses for the outer planetesimal disk
    nb_co : integer
        Number of outer planetesimals

    Returns
    -------
    M_co: float
        Mass of outer planetesimal

    """
    M_earth_SU = M_earth.value / M_sun.value  # Earth's mass in solar units
    M_tot = nbc_Earth * M_earth_SU  # Total mass outer planetesimals in solar units (nbc_Earth is the mass of the comets in earth masses)

    M_co = M_tot / nb_co  # Mass of each outer planetesimal
    
    return M_co

def a_annulus(r_in, r_out, n):
    """
    Generates n random semi-major axes (uniformly distributed) within an annulus of 
    inner radius r_in and outer radius r_out
    Case: "Annulus" (Grand Tack, Broz et al., Low-mass asteroid belt)

    Parameters
    ----------
    r_in : float
        Inner radius of the annulus of terrestrial protoplanets / planetesimals
    r_out : float
        Outer radius of the annulus of terrestrial protoplanets / planetesimals
    n : integer
        Number of terrestrial protoplanets / planetesimals

    Returns
    -------
    a: float
        semi-major axes of the terrestrial protoplanets

    """
    a = np.random.uniform(r_in, r_out, n)
    return a

def e_i_generator(n):
    """
    Generates n random eccentricities and inclinations (following a Raileigh distribution)
    cf. what has been done in Nesvorny et al. (2020), and also Minton & Levison (2014)

    Parameters
    ----------
    n : integer
        Number of terrestrial protoplanets / planetesimals

    Returns
    -------
    e, i: float
        eccentricty and inclination of the terrestrial protoplanets

    """
    e = np.random.rayleigh(0.005, n) # The value of 0.005 is taken from Nesvorny et al. (2020)
    i = np.random.rayleigh(0.0025, n) # The value of 0.0025 is taken from Nesvorny et al. (2020)
    return e, i

def g_n_generator(n):
    """
    Generates n random arguments of pericentre and longitudes of ascending node 
    uniformly distributed between 0 and 360 degrees (0 is included and 360 is excluded)
    
    Parameters
    ----------
    n : integer
        Number of terrestrial protoplanets / planetesimals

    Returns
    -------
    g, n: float
        argument of pericentre (in degrees) and longitude of ascending node (in degrees)

    """
    g = np.random.uniform(0, 360, n)
    n = np.random.uniform(0, 360, n)
    return g, n

def M_generator(n):
    """
    Generates n random mean anomalies uniformly distributed between 0 and 360 degrees 
    (0 is included and 360 is excluded)
    
    Parameters
    ----------
    n : integer
        Number of terrestrial protoplanets / planetesimals

    Returns
    -------
    M: float
         mean anomaly (in degrees)

    """
    M = np.random.uniform(0, 360, n)
    return M

def kep_2_cart(a, e, i, g, n, M):
    """
    Converts Keplerian coordinates to cartesian coordinates

    Parameters
    ----------
    a : float
        semi-major axis (in AU)
    e : float
        eccentricty
    i : float
        inclination (degrees)
    g : float
        argument of pericentre (degrees) w
    n : float
        longitude of ascending node (degrees) O
    M : float
        mean anomaly (degrees) ≃ true anomaly for circular orbits

    Returns
    -------
    x, y, z, vx, vy, vz: float
        cartesian coordinates of the protoplanets / planetesimals 

    """
    # Convert degrees into radians
    i = np.deg2rad(i)
    g = np.deg2rad(g)
    n = np.deg2rad(n)
    M = np.deg2rad(M)
    
    # Compute the radius r in AU
    r= a * (1 - e**2) / (1 + (e * np.cos(M))) 
    
    # Compute the specific angular momentum h
    mu_ms = G.value * M_sun.value # Standard gravitational parameter = GM of the central body
                                  # with G the Newtonian constant of gravitation (in m^3s^-2) 
    mu = mu_ms * (86400**2) / ((au.value)**3)    # There are 86400 s in a Julian day                         
    h = np.sqrt(mu * a *(1 - e**2)) 
    
    # Compute the position components x, y, z (in AU)
    x = r * (np.cos(n) * np.cos(g + M) - np.sin(n) * np.sin(g + M) * np.cos(i))
    y = r * (np.sin(n) * np.cos(g + M) + np.cos(n) * np.sin(g + M) * np.cos(i))
    z = r * np.sin(i) * np.sin(g + M) 
    
    # Compute the velocity components vx, vy, vz (in AU/day)
    vx = (x * h * e * np.sin(M) / (r * a * (1 - e**2))) - h * (np.cos(n) * np.sin(g + M) + np.sin(n) * np.cos(g + M) * np.cos(i)) / r
    vy = (y * h * e * np.sin(M) / (r * a * (1 - e**2))) - h * (np.sin(n) * np.sin(g + M) - np.cos(n) * np.cos(g + M) * np.cos(i)) / r
    vz = (z * h * e * np.sin(M) / (r * a * (1 - e**2))) + h * np.sin(i) * np.cos(g + M) / r 
    return x, y, z, vx, vy, vz

def xy_generator(r_in, r_out, n):
    """
    Generates n random x and y coordinates (uniformly distributed) within an annulus of 
    inner radius r_in and outer radius r_out
    => might not be useful if we generate keplerian coordinates and then convert it into 
    cartesian coordinates

    Parameters
    ----------
    n : integer
        Number of terrestrial protoplanets / planetesimals
    r_in : float
        Inner radius of the annulus of terrestrial protoplanets / planetesimals
    r_out : TYPE
        Outer radius of the annulus of terrestrial protoplanets / planetesimals

    Returns
    -------
    x, y : float
        x and y coordinates of the terrestrial planetesimals

    """
    angle = np.random.uniform(0., 2 * pi, n) # In radians
    r_squared = np.random.uniform(r_in ** 2, r_out ** 2, n)  # r_in^2 ≤ r^2 ≤ r_out^2  and r^2 = x^2 + y^2 
    r = np.sqrt(r_squared)
    x = r * np.cos(angle)
    y = r * np.sin(angle)
    return x, y

def z_generator(mu, sigma, n):
    """
    Generates n random z coordinates (normally distributed)
    => might not be useful if we generate keplerian coordinates and then convert it into 
    cartesian coordinates

    Parameters
    ----------
    mu: float
        Mean of the normal distribution
    sigma: float
        Standart deviation of the normal distribution
    n : integer
        Number of terrestrial protoplanets / planetesimals

    Returns
    -------
    z : float
        z coordinates of the terrestrial protoplanets / planetesimals 

    """
    z = np.random.normal(mu, sigma, n)
    return z

def vel_generator(mu, sigma, n):
    """
    Generates n random vx, vy and vz coordinates (normally distributed)
    => might not be useful if we generate keplerian coordinates and then convert it into 
    cartesian coordinates

    Parameters
    ----------
    mu: float
        Mean of the normal distribution
    sigma: float
        Standart deviation of the normal distribution
    n : integer
        Number of terrestrial protoplanets / planetesimals

    Returns
    -------
    vx, vy, vz : float
        vx, vy, vz coordinates of the terrestrial protoplanets / planetesimals 

    """
    vx = np.random.normal(mu, sigma, n)
    vy = np.random.normal(mu, sigma, n)
    vz = np.random.normal(mu, sigma, n)
    return vx, vy, vz

# ------------------------------------------------------------------------------------------------
# Define default input values
# ------------------------------------------------------------------------------------------------

# Configuration of the inner solar system
PROTOPLANETS = True
PLANETESIMALS = True 
BIG = True  # True if case big, false if case small
if BIG == True:
    SMALL = False
else:
    SMALL = True
    
# Configuration of the giant planets (for no giant planet whatsoever, put False for each case)
GIANT_cart_actual = False # never used
GIANT_kep_actual = False # jovonly and jovi cases
GIANT_kep_set = False # jovset case
GIANT_Clement = False # Clement case
GIANT_Deienno = True # Deienno case
GIANT_Nesvorny = False # Nesvorny case

# Configuration of the comets
COMETS = True

# Input parameters for the terrestrial protoplanets and planetesimals; and the comets (can be changed)
if BIG:
    nb_pp = 20   # Number of protoplanets = 20 in the "big case"
if SMALL:
    nb_pp = 40   # Number of protoplanets = 40 in the "small case" 
nb_pl = 500   # Number of inner planetesimals
nb_co = 10000   # Number of outer planetesimals (comets)

nb_Earth = 2.5   # Number of Earth masses for the terrestrial protoplanets (embryos) and the planetesimals
r_in = 0.7   # In AU (for the inner disk)
r_out = 1.2   # In AU (for the inner disk)

nbc_Earth = 25    # Number of Earth masses for the outer planetesimals (comets)
rc_in = 21   # In AU (for the outer disk)
rc_out = 30   # In AU (for the outer disk)

# Input values for the terrestrial protoplanets and planetesimals; and the comets (should not be changed)
r_pp = '1.00000e+00' # Max distance from a protoplanet (in Hill raddii) that constitutes a close encounter
d_pp = '3.00000e+00' # Density of the protoplanets in g/cm^3  
d_pl = '2.00000e+00' # Density of the inner planetesimals in g/cm^3  
d_co = '0.50000e+00' # Density of the outer planetesimals (comets) in g/cm^3 
M_pp = str(mass_generator(nb_Earth, nb_pp, nb_pl)[0])  # Mass of each protoplanet in Solar Masses
M_pl = str(mass_generator(nb_Earth, nb_pp, nb_pl)[1])  # Mass of each planetesimal in Solar Masses
M_co = str(massc_generator(nbc_Earth, nb_co)) # Mass of each outer planetesimal (comet) in Solar Masses
S_pp = '0.0'


# =================================================================================================
# In this section 10 folders are generated in a specific directory (the directory should be changed depending on
# the case: big or small)
# -------------------------------------------------------------------------------------------------
for nb in range(1, 11):
    def_dir = "/Users/sarahjoiret/Desktop/sims/genga/case2/big/deienno/10000comets/"
    os.mkdir(def_dir + str(nb))
    default_out_dir = def_dir + str(nb)
    
    # ------------------------------------------------------------------------------------------------
    # Copy all the input files necessary for a genga run in each 10 folders 
    # param.dat and genga.job are in the input_files folder
    # ------------------------------------------------------------------------------------------------
    source_dir = "/Users/sarahjoiret/Desktop/sims/genga/input_files/"
    
    # Fetch all files
    for filename in os.listdir(source_dir):
        source = source_dir + filename
        destination = default_out_dir + "/" + filename
        
        # Copy only files 
        if os.path.isfile(source):
            shutil.copy(source, destination)
            
    # =================================================================================================
    # In this section the init.dat files are generated
    # ------------------------------------------------------------------------------------------------
    # Create the init.dat file in a specific directory, open it and write in it 
    # ------------------------------------------------------------------------------------------------
    default_file_name = "initial.dat"
    file_path = os.path.join(default_out_dir, default_file_name)
    file_init = open(file_path, "w+")

    # ------------------------------------------------------------------------------------------------
    # Create a dictionary for the giant planets 
    # For GIANT_actual: taken from the Horizon jpl website for the cartesian coordinates (https://ssd.jpl.nasa.gov/horizons/app.html#/) on 2022-Apr-12 00:00:00.0000
    # (r indicates the max distance from the body (in Hill raddii) that constitutes a close encounter (default is r=1))
    # d indicates the density of the body in g/cm^3 (default is d=1)
    # m indicates the body's mass in Solar masses (if a value is not specified, the mass is assumed to be 0)
    # ------------------------------------------------------------------------------------------------
    
    if GIANT_cart_actual:
        giant_actual_dict = {}
        
        giant_actual_dict['d'] = np.array(['1.00000E+00', '1.00000E+00', '1.00000E+00', '1.00000E+00'])
        giant_actual_dict['m'] = np.array(['0.954324237111000E-03', '0.285716656000000E-03', '0.436500000000000E-04', '0.514900000000000E-04'])
        giant_actual_dict['x'] = np.array(['4.856667788838316E+00', '7.310566318127806E+00', '1.411169967197528E+01', '2.966424563924522E+01'])
        giant_actual_dict['y'] = np.array(['-1.025488061152929E+00', '-6.658412608528993E+00', '1.375076475280445E+01', '-3.771098136105441E+00'])
        giant_actual_dict['z'] = np.array(['-1.044013501452873E-01', '-1.752918068843707E-01', '-1.317490569422967E-01', '-6.059837711964712E-01'])
        #giant_actual_dict['vx'] = np.array(['1.469214244356701E-03', '3.443626331268295E-03', '-2.773830926096617E-03', '3.754365324365258E-04'])
        #giant_actual_dict['vy'] = np.array(['7.736375041500906E-03', '4.113765988889820E-03', '2.633722272323927E-03', '3.133090911420504E-03'])
        #giant_actual_dict['vz'] = np.array(['-6.500748338995526E-05', '-2.088257913410473E-04', '4.573510209027578E-05', '-7.307704001256925E-05'])
        
        # We divide all the velocities by 0.0172020989 because of the units in genga (G=1)
        giant_actual_dict['vx'] = np.array(['8.540901043356701E-02', '2.001864047268295E-01', '-1.612495628096617E-01', '2.182504208365258E-02'])
        giant_actual_dict['vy'] = np.array(['4.497343659500906E-01', '2.391432588889820E-01', '1.531047047323927E-01', '1.821342227420504E-01'])
        giant_actual_dict['vz'] = np.array(['-3.779043695995526E-03', '-1.213955313410473E-02', '2.658693126027578E-03', '-4.248146721256925E-03'])
        
        giant_actual_dict['Sx'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        giant_actual_dict['Sy'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        giant_actual_dict['Sz'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        
        # Write the cartesian coordinates of the giant planets in the initial.dat file
        for i in range(len(giant_actual_dict['d'])):
            file_init.write(giant_actual_dict['d'][i] + ' ' + giant_actual_dict['m'][i] + ' ' \
                           + giant_actual_dict['x'][i] + ' ' + giant_actual_dict['y'][i] + ' ' + giant_actual_dict['z'][i] + ' ' \
                               + giant_actual_dict['vx'][i] + ' ' + giant_actual_dict['vy'][i] + ' ' + giant_actual_dict['vz'][i] + ' '\
                                   + giant_actual_dict['Sx'][i] + ' ' + giant_actual_dict['Sy'][i] + ' ' + giant_actual_dict['Sz'][i] + '\n') 
    
    if GIANT_kep_actual:
        giant_actual_dict = {}
        
        giant_actual_dict['d'] = np.array(['1.00000E+00', '1.00000E+00', '1.00000E+00', '1.00000E+00'])
        giant_actual_dict['m'] = np.array(['0.954324237111000E-03', '0.285716656000000E-03', '0.436500000000000E-04', '0.514900000000000E-04'])
        giant_actual_dict['a'] = np.array(['5.203363011111316E+00', '9.537070321111316E+00', '1.919126393111128E+01', '3.006896348111122E+01'])
        giant_actual_dict['e'] = np.array(['4.853611397496070E-02', '5.287787550700090E-02', '4.384538567844176E-02', '1.435471124865085E-02'])
        giant_actual_dict['inc'] = np.array(['1.303512972858737E+00', '2.489561917163449E+00', '7.719204090707306E-01', '1.767861617962627E+00'])

        giant_actual_dict['O'] = np.array(['1.005092657202147E+02', '1.136313424807445E+02', '7.402895319751609E+01', '1.317130739792393E+02'])
        giant_actual_dict['w'] = np.array(['2.733542004407939E+02', '3.348985231507333E+02', '9.409835961993808E+01', '2.491591365066763E+02'])
        giant_actual_dict['M'] = np.array(['3.402922896972250E+02', '2.353888353884061E+02', '2.408769775500852E+02', '3.329273246801988E+02'])
        
        giant_actual_dict['Sx'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        giant_actual_dict['Sy'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        giant_actual_dict['Sz'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        
        # Write the cartesian coordinates of the giant planets in the initial.dat file
        for i in range(len(giant_actual_dict['d'])):
            file_init.write(giant_actual_dict['d'][i] + ' ' + giant_actual_dict['m'][i] + ' ' \
                           + giant_actual_dict['a'][i] + ' ' + giant_actual_dict['e'][i] + ' ' + giant_actual_dict['inc'][i] + ' ' \
                               + giant_actual_dict['O'][i] + ' ' + giant_actual_dict['w'][i] + ' ' + giant_actual_dict['M'][i] + ' '\
                                   + giant_actual_dict['Sx'][i] + ' ' + giant_actual_dict['Sy'][i] + ' ' + giant_actual_dict['Sz'][i] + '\n') 
                
    if GIANT_kep_set:
        # These values are taken from the output of the setelement_jovset.py routine which converts the cartesian coord of jovonly in keplerian coord
        giant_actual_dict = {}
        
        giant_actual_dict['d'] = np.array(['1.00000E+00', '1.00000E+00', '1.00000E+00', '1.00000E+00'])
        giant_actual_dict['m'] = np.array(['0.954324237111000E-03', '0.285716656000000E-03', '0.436500000000000E-04', '0.514900000000000E-04'])
        giant_actual_dict['a'] = np.array(['5.208806789096323E+00', '9.539627718247774E+00', '1.9192064387012977E+01', '3.007055177895079E+01'])
        giant_actual_dict['e'] = np.array(['4.947040864113751E-02', '5.271204679697842E-02', '4.38232898216967E-02', '1.4401003774403823E-02'])
        giant_actual_dict['inc'] = np.array(['1.303512972858737E+00', '2.489561917163449E+00', '7.719204090707306E-01', '1.767861617962627E+00'])

        giant_actual_dict['O'] = np.array(['1.005092657202147E+02', '1.136313424807445E+02', '7.402895319751609E+01', '1.317130739792393E+02'])
        giant_actual_dict['w'] = np.array(['2.733542004407939E+02', '3.348985231507333E+02', '9.409835961993808E+01', '2.491591365066763E+02'])
        giant_actual_dict['M'] = np.array(['3.402922896972250E+02', '2.353888353884061E+02', '2.408769775500852E+02', '3.329273246801988E+02'])
        
        giant_actual_dict['Sx'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        giant_actual_dict['Sy'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        giant_actual_dict['Sz'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        
        # Write the cartesian coordinates of the giant planets in the initial.dat file
        for i in range(len(giant_actual_dict['d'])):
            file_init.write(giant_actual_dict['d'][i] + ' ' + giant_actual_dict['m'][i] + ' ' \
                           + giant_actual_dict['a'][i] + ' ' + giant_actual_dict['e'][i] + ' ' + giant_actual_dict['inc'][i] + ' ' \
                               + giant_actual_dict['O'][i] + ' ' + giant_actual_dict['w'][i] + ' ' + giant_actual_dict['M'][i] + ' '\
                                   + giant_actual_dict['Sx'][i] + ' ' + giant_actual_dict['Sy'][i] + ' ' + giant_actual_dict['Sz'][i] + '\n') 
    if GIANT_Clement:
        # These values are taken from Matt Clement's instability data at time 0 
        giant_actual_dict = {}
        
        giant_actual_dict['d'] = np.array(['1.00000E+00', '1.00000E+00', '1.00000E+00', '1.00000E+00', '1.00000E+00'])
        giant_actual_dict['m'] = np.array(['9.543242371110001E-04', '2.8571665599999997E-04', '2.4048000000000004E-05', '4.8048000000000005E-05', '4.8048000000000005E-05'])
        giant_actual_dict['a'] = np.array(['5.606470000000000E+00', '8.984628000000000E+00', '1.260955000000000E+01', '1.655826000000000E+01', '2.165805000000000E+01'])
        giant_actual_dict['e'] = np.array(['9.011960000000000E-02', '2.24060000000000E-01', '4.63419000000000E-02', '2.02791000000000E-02', '2.01639000000000E-02'])
        giant_actual_dict['inc'] = np.array(['0.121800000000000E+00', '0.156200000000000E+00', '0.329700000000000E+00', '0.302000000000000E+00', '0.229400000000000E+00'])

        giant_actual_dict['O'] = np.array(['1.603466000000000E+02', '1.223872000000000E+02', '2.207940000000000E+02', '2.873303000000000E+02', '1.920886000000000E+02'])
        giant_actual_dict['w'] = np.array(['3.306533000000000E+02', '2.478096000000000E+02', '3.257145000000000E+02', '3.443483000000000E+02', '2.396214000000000E+02'])
        giant_actual_dict['M'] = np.array(['3.597155000000000E+02', '9.575680000000000E+01', '2.794331000000000E+02', '2.207451000000000E+02', '3.376700000000000E+02'])
        
        giant_actual_dict['Sx'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        giant_actual_dict['Sy'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        giant_actual_dict['Sz'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        
        # Write the cartesian coordinates of the giant planets in the initial.dat file
        for i in range(len(giant_actual_dict['d'])):
            file_init.write(giant_actual_dict['d'][i] + ' ' + giant_actual_dict['m'][i] + ' ' \
                           + giant_actual_dict['a'][i] + ' ' + giant_actual_dict['e'][i] + ' ' + giant_actual_dict['inc'][i] + ' ' \
                               + giant_actual_dict['O'][i] + ' ' + giant_actual_dict['w'][i] + ' ' + giant_actual_dict['M'][i] + ' '\
                                   + giant_actual_dict['Sx'][i] + ' ' + giant_actual_dict['Sy'][i] + ' ' + giant_actual_dict['Sz'][i] + '\n')
                
        
    if GIANT_Deienno:
        # These values are taken from Deienno's instability data at time 0. For Jupiter, Saturn, Ice1, Ur and Nep taken from jup/sat/ice/ura/nep-0765-inst.data 
        # argument of pericenter w and mean anomaly M are obtained from longitude of pericentre and lambda given in Deienno's data
        giant_actual_dict = {}
        
        giant_actual_dict['d'] = np.array(['1.00000E+00', '1.00000E+00', '1.00000E+00', '1.00000E+00', '1.00000E+00'])
        giant_actual_dict['m'] = np.array(['9.55E-04', '2.84E-04', '4.5E-05', '4.805E-05', '4.805E-05'])
        giant_actual_dict['a'] = np.array(['0.535597E+01', '0.764851E+01', '0.108598E+02', '0.169067E+02', '0.278967E+02'])
        giant_actual_dict['e'] = np.array(['0.155775E-01', '0.451761E-01', '0.977566E-01', '0.122321E-01', '0.680936E-02'])
        giant_actual_dict['inc'] = np.array(['0.580096E-02', '0.215198E+00', '0.284031E+00', '0.353495E+00', '0.159401E+00'])

        giant_actual_dict['O'] = np.array(['0.121475E+03', '0.300528E+03', '0.188293E+03', '0.249591E+02', '0.144050E+03'])
        giant_actual_dict['w'] = np.array(['245.48677', '276.484', '215.2122', '205.8089', '355.15'])
        giant_actual_dict['M'] = np.array(['32.91503', '214.4024', '129.9948', '319.611', '335.595'])
        
        giant_actual_dict['Sx'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        giant_actual_dict['Sy'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        giant_actual_dict['Sz'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        
        # Write the cartesian coordinates of the giant planets in the initial.dat file
        for i in range(len(giant_actual_dict['d'])):
            file_init.write(giant_actual_dict['d'][i] + ' ' + giant_actual_dict['m'][i] + ' ' \
                           + giant_actual_dict['a'][i] + ' ' + giant_actual_dict['e'][i] + ' ' + giant_actual_dict['inc'][i] + ' ' \
                               + giant_actual_dict['O'][i] + ' ' + giant_actual_dict['w'][i] + ' ' + giant_actual_dict['M'][i] + ' '\
                                   + giant_actual_dict['Sx'][i] + ' ' + giant_actual_dict['Sy'][i] + ' ' + giant_actual_dict['Sz'][i] + '\n')
                
    if GIANT_Nesvorny: 
        # These values are taken from Nesvorny's instability data at time zero. 
        # The masses of the ice giants are the same as in Deienno's case
        # The ejected ice giant is at the fourth position!!! 
        
        giant_actual_dict = {}
        
        giant_actual_dict['d'] = np.array(['1.00000E+00', '1.00000E+00', '1.00000E+00', '1.00000E+00', '1.00000E+00'])
        giant_actual_dict['m'] = np.array(['9.54E-04', '2.85E-04', '4.805E-05', '4.5E-05', '4.805E-05'])
        giant_actual_dict['a'] = np.array(['5.46945715', '7.45693731', '0.108598E+02', '16.0804615', '22.1722355'])
        giant_actual_dict['e'] = np.array(['0.00345820561', '0.0105128186', '0.0166763719', '0.00612786878', '0.00208495045'])
        giant_actual_dict['inc'] = np.array(['0.000862089277', '0.000304975256', '0.00192281045', '0.00117235875', '0.000874781341'])

        giant_actual_dict['O'] = np.array(['266.91', '136.93', '309.44', '20.76', '245.71'])
        giant_actual_dict['w'] = np.array(['153.19', '105.55', '263.92', '21.73', '228.97'])
        giant_actual_dict['M'] = np.array(['64.79', '106.94', '164.98', '4.11', '148.0'])
        
        giant_actual_dict['Sx'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        giant_actual_dict['Sy'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        giant_actual_dict['Sz'] = np.array(['0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00', '0.000000000000000E+00'])
        
        # Write the cartesian coordinates of the giant planets in the initial.dat file
        for i in range(len(giant_actual_dict['d'])):
            file_init.write(giant_actual_dict['d'][i] + ' ' + giant_actual_dict['m'][i] + ' ' \
                           + giant_actual_dict['a'][i] + ' ' + giant_actual_dict['e'][i] + ' ' + giant_actual_dict['inc'][i] + ' ' \
                               + giant_actual_dict['O'][i] + ' ' + giant_actual_dict['w'][i] + ' ' + giant_actual_dict['M'][i] + ' '\
                                   + giant_actual_dict['Sx'][i] + ' ' + giant_actual_dict['Sy'][i] + ' ' + giant_actual_dict['Sz'][i] + '\n')
        
        
        
            
    # ------------------------------------------------------------------------------------------------
    # Create a dictionary for the protoplanets
    # ------------------------------------------------------------------------------------------------ 
    if PROTOPLANETS:
        pp_dict = {}

        # Generate the lists of d, m, a, e, i, g (argument of pericenter), n (longitude of ascending node), M (mean anomaly), S (spin)
        list_of_d = [d_pp for i in range(nb_pp)]
        list_of_m = [M_pp for i in range(nb_pp)]
        list_of_a = np.array(a_annulus(r_in, r_out, nb_pp))
        list_of_e = np.array(e_i_generator(nb_pp)[0])
        list_of_i = np.array(e_i_generator(nb_pp)[1])
        list_of_g = np.array(g_n_generator(nb_pp)[0])
        list_of_n = np.array(g_n_generator(nb_pp)[1])
        list_of_M = np.array(M_generator(nb_pp))
        list_of_S = [S_pp for i in range(nb_pp)]
        
        # In the dictionnary, put the lists as arrays of strings
        pp_dict['d'] = np.array(list_of_d)
        pp_dict['m'] = np.array(list_of_m)
        pp_dict['a'] = np.asarray(list_of_a, dtype=str)
        pp_dict['e'] = np.asarray(list_of_e, dtype=str)
        pp_dict['i'] = np.asarray(list_of_i, dtype=str)
        pp_dict['O'] = np.asarray(list_of_g, dtype=str)
        pp_dict['w'] = np.asarray(list_of_n, dtype=str)
        pp_dict['M'] = np.asarray(list_of_M, dtype=str)
        pp_dict['Sx'] = np.array(list_of_S)
        pp_dict['Sy'] = np.array(list_of_S)
        pp_dict['Sz'] = np.array(list_of_S)

        # Write the keplerian coordinates of the terrestrial protoplanets in the init.dat file
        for i in range(len(pp_dict['d'])):
            file_init.write(pp_dict['d'][i] + ' ' + pp_dict['m'][i] + ' ' \
                           + pp_dict['a'][i] + ' ' + pp_dict['e'][i] + ' ' + pp_dict['i'][i] + ' ' \
                               + pp_dict['O'][i] + ' ' + pp_dict['w'][i] + ' ' + pp_dict['M'][i] + ' ' \
                                   + pp_dict['Sx'][i] + ' ' + pp_dict['Sy'][i] + ' ' + pp_dict['Sz'][i] + '\n')
        
            
    # ------------------------------------------------------------------------------------------------
    # Create a dictionary for the inner planetesimals
    # ------------------------------------------------------------------------------------------------ 
    
    if PLANETESIMALS:
        pl_dict = {}

        # Generate the lists of d, m, a, e, i, g (argument of pericenter), n (longitude of ascending node), M (mean anomaly), S (spin)
        list_of_d = [d_pl for i in range(nb_pl)]  # Density of planetesimals is considered to be the same as density of protoplanets
        list_of_m = [M_pl for i in range(nb_pl)]
        list_of_a = np.array(a_annulus(r_in, r_out, nb_pl))
        list_of_e = np.array(e_i_generator(nb_pl)[0])
        list_of_i = np.array(e_i_generator(nb_pl)[1])
        list_of_g = np.array(g_n_generator(nb_pl)[0])
        list_of_n = np.array(g_n_generator(nb_pl)[1])
        list_of_M = np.array(M_generator(nb_pl))
        list_of_S = [S_pp for i in range(nb_pl)]
        
        # In the dictionnary, put the lists as arrays of strings
        pl_dict['d'] = np.array(list_of_d)
        pl_dict['m'] = np.array(list_of_m)
        pl_dict['a'] = np.asarray(list_of_a, dtype=str)
        pl_dict['e'] = np.asarray(list_of_e, dtype=str)
        pl_dict['i'] = np.asarray(list_of_i, dtype=str)
        pl_dict['O'] = np.asarray(list_of_g, dtype=str)
        pl_dict['w'] = np.asarray(list_of_n, dtype=str)
        pl_dict['M'] = np.asarray(list_of_M, dtype=str)
        pl_dict['Sx'] = np.array(list_of_S)
        pl_dict['Sy'] = np.array(list_of_S)
        pl_dict['Sz'] = np.array(list_of_S)      

        # Write the keplerian coordinates of the planetesimals in the init.dat file
        for i in range(len(pl_dict['d'])):
            file_init.write(pl_dict['d'][i] + ' ' + pl_dict['m'][i] + ' ' \
                           + pl_dict['a'][i] + ' ' + pl_dict['e'][i] + ' ' + pl_dict['i'][i] + ' ' \
                               + pl_dict['O'][i] + ' ' + pl_dict['w'][i] + ' ' + pl_dict['M'][i] + ' ' \
                                   + pl_dict['Sx'][i] + ' ' + pl_dict['Sy'][i] + ' ' + pl_dict['Sz'][i] + '\n') 
        

    
           
    # ------------------------------------------------------------------------------------------------
    # Create a dictionary for the outer planetesimals (comets)
    # ------------------------------------------------------------------------------------------------ 
    
    if COMETS:
        
        co_dict = {}

        # Generate the lists of d, m, a, e, i, g (argument of pericenter), n (longitude of ascending node), M (mean anomaly), S (spin)
        list_of_d = [d_co for i in range(nb_co)]  # Density of planetesimals is considered to be the same as density of protoplanets
        list_of_m = [M_co for i in range(nb_co)]
        list_of_a = np.array(a_annulus(rc_in, rc_out, nb_co))
        list_of_e = np.array(e_i_generator(nb_co)[0])
        list_of_i = np.array(e_i_generator(nb_co)[1])
        list_of_g = np.array(g_n_generator(nb_co)[0])
        list_of_n = np.array(g_n_generator(nb_co)[1])
        list_of_M = np.array(M_generator(nb_co))
        list_of_S = [S_pp for i in range(nb_co)]
        
        # In the dictionnary, put the lists as arrays of strings
        co_dict['d'] = np.array(list_of_d)
        co_dict['m'] = np.array(list_of_m)
        co_dict['a'] = np.asarray(list_of_a, dtype=str)
        co_dict['e'] = np.asarray(list_of_e, dtype=str)
        co_dict['i'] = np.asarray(list_of_i, dtype=str)
        co_dict['O'] = np.asarray(list_of_g, dtype=str)
        co_dict['w'] = np.asarray(list_of_n, dtype=str)
        co_dict['M'] = np.asarray(list_of_M, dtype=str)
        co_dict['Sx'] = np.array(list_of_S)
        co_dict['Sy'] = np.array(list_of_S)
        co_dict['Sz'] = np.array(list_of_S) 
        
        # Write the keplerian coordinates of the planetesimals in the init.dat file
        for i in range(len(co_dict['d'])):
            file_init.write(co_dict['d'][i] + ' ' + co_dict['m'][i] + ' ' \
                           + co_dict['a'][i] + ' ' + co_dict['e'][i] + ' ' + co_dict['i'][i] + ' ' \
                               + co_dict['O'][i] + ' ' + co_dict['w'][i] + ' ' + co_dict['M'][i] + ' ' \
                                   + co_dict['Sx'][i] + ' ' + co_dict['Sy'][i] + ' ' + co_dict['Sz'][i] + '\n')
        
     
            
    # ------------------------------------------------------------------------------------------------
    # Close the initial.dat file 
    # ------------------------------------------------------------------------------------------------     
    file_init.close()