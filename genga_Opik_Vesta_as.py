"""
Calculate the probability of collision between a Vesta-like planetesimal and an asteroid with the Opik - Wetherill method
(Wetherill, 1967) including considerations from Greenberg (1982) and Farinella and Davis (1992).
I translated this code (from Paolo Farinella's group) from Fortran to Python and parallelized it, 
and I also included considerations from Bottke et al. (1994) so that collision velocities are weighted by the 
collision probability per time at each orientation.

author: sarahjoiret
"""
import numpy as np
import math
import glob
import os
from decimal import Decimal, DivisionUndefined, InvalidOperation
from astropy.constants import G, M_sun, au
import multiprocessing
import pathos
from pathos.multiprocessing import ProcessingPool

# Define the path (to be changed)
BASE_PATH = "/gpfs/home/sjoiret/genga/case1/1"
OUTPUT_DIR = os.path.join(BASE_PATH, "collision_proba_Vesta_as")

def cart_2_kep(x, y, z, vx, vy, vz):
    """
    Converts cartesian coordinates to Keplerian coordinates

    Parameters
    ----------
    x : float
        position (x-axis)
    y : float
        position (y-axis)
    z : float
        position (z-axis)
    vx : float
        velocity (x-axis) in AU/day (has already been multiplied by 0.01720209895 to be converted in AU/day)
    vy : float
        velocity (y-axis) in AU/day (has already been multiplied by 0.01720209895 to be converted in AU/day)
    vz : float
        velocity (z-axis) in AU/day (has already been multiplied by 0.01720209895 to be converted in AU/day)

    Returns
    -------
    a, e, cosi: float
        Keplerian coordinates of the protoplanets / planetesimals 

    """
    
    # Compute the radius, r, and velocity v
    r = np.sqrt(x**2 + y**2 + z**2)
    v = np.sqrt(vx**2 + vy**2 + vz**2)
    
    # Compute vector r and vector v
    r_vector = np.array([x, y, z])
    v_vector = np.array([vx, vy, vz])
    
    # Compute the specific angular momentum
    h_vector = np.cross(r_vector, v_vector)
    h = np.sqrt(h_vector[0]**2 + h_vector[1]**2 + h_vector[2]**2)
    
    # Compute the Standard gravitational parameter in AU/day
    mu_ms = G.value * M_sun.value # Standard gravitational parameter = GM of the central body
                                  # with G the Newtonian constant of gravitation (in m^3s^-2) 
    mu = mu_ms * (86400**2) / ((au.value)**3)    # There are 86400 s in a Julian day 
    
    # Compute the specific energy E
    E = ((v**2) / 2) - (mu / r)
    
    # Compute semi-major axis a
    a = - mu / (2 * E)
    
    # Compute eccentricity e
    e = np.sqrt(1 - (h**2 / (a * mu)))
    
    # Compute cosi
    cosi = h_vector[2]/h
    
    return a, e, cosi

class body:
    time:np.ndarray
    a:np.ndarray
    e:np.ndarray
    cosi:np.ndarray
    inc:np.ndarray
    q:np.ndarray
    qq:np.ndarray
    x:np.ndarray
    y:np.ndarray
    z:np.ndarray
    vx:np.ndarray
    vy:np.ndarray
    vz:np.ndarray
    mass:np.ndarray
    radius:np.ndarray
    idx:np.ndarray
    
    def __init__(self, filename: str):
        # Load the data from the Out*.dat file
        data = np.loadtxt(filename)
        # If only one line in the Out*.dat, the data are not a 2-D list and the values
        # must be loaded differently
        if data.shape == (21,):
            self.time = np.asarray([data[0]])
            self.idx = np.asarray([data[1]])
            self.mass = np.asarray([data[2]])   # in solar masses
            self.radius = np.asarray([data[3]]) # in AU
            self.radius_m = np.asarray([data[3]*au.value]) # in meters (not km!!)
            self.x = np.asarray([data[4]])
            self.y = np.asarray([data[5]])
            self.z = np.asarray([data[6]])
            self.vx = np.asarray([data[7]*0.01720209895]) # Convert the units of the velocity from AU/day' to AU/day
            self.vy = np.asarray([data[8]*0.01720209895]) # Convert the units of the velocity from AU/day' to AU/day
            self.vz = np.asarray([data[9]*0.01720209895]) # Convert the units of the velocity from AU/day' to AU/day
            
        else:
            self.time = data[:,0]
            self.idx = data[1,1]
            self.mass = data[:,2]  # in solar masses
            self.radius = data[:,3] # in AU
            self.radius_m = data[:,3]*au.value # in meters (not km!!)
            self.x = data[:,4]
            self.y = data[:,5]
            self.z = data[:,6]
            self.vx = data[:,7]*0.01720209895  # Convert the units of the velocity from AU/day' to AU/day
            self.vy = data[:,8]*0.01720209895  # Convert the units of the velocity from AU/day' to AU/day
            self.vz = data[:,9]*0.01720209895  # Convert the units of the velocity from AU/day' to AU/day

        self.filename = os.path.splitext(os.path.basename(filename))[0]
        self.len_time = len(self.time)
        self.a = np.zeros(self.len_time)
        self.e = np.zeros(self.len_time)
        self.cosi = np.zeros(self.len_time)
        self.inc = np.zeros(self.len_time)
        self.q = np.zeros(self.len_time)
        self.qq = np.zeros(self.len_time)
        for i in range(self.len_time):
            self.a[i] = cart_2_kep(self.x[i], self.y[i], self.z[i], self.vx[i], self.vy[i], self.vz[i])[0]
            self.e[i] = cart_2_kep(self.x[i], self.y[i], self.z[i], self.vx[i], self.vy[i], self.vz[i])[1]
            self.cosi[i] = cart_2_kep(self.x[i], self.y[i], self.z[i], self.vx[i], self.vy[i], self.vz[i])[2]
            self.inc[i] = np.arccos(self.cosi[i])  # in radians
            self.q[i] = self.a[i] * (1 - self.e[i])
            self.qq[i] = self.a[i] * (1 + self.e[i])


# -----------------------------------------------------------------------------
# Opik method:
# -----------------------------------------------------------------------------
def cp_comp(A1OR, A2OR, x1, x2, e1, e2, r, relinc, c):
    """
    Function used in call_collprb
    Parameters
    ----------
    A1OR, A2OR : float
        a1/r and a2/r
    x1, x2 : float
        defined in call_collprb
    e1, e2 : float
        eccentricty of body and comet respectively
    r : float
        orbital distance
    relinc : float
        relative inclination
    c : float
        defined in call_collprb
    Returns
    -------
    P12, U12: float
    """
    comp1 = np.sqrt(x1/((A1OR**2)*(1. - e1**2)))
    comp2 = np.sqrt(x2/((A2OR**2)*(1. - e2**2)))
    P = np.array([0., 0.])
    Urel = np.array([0., 0.])
    G_value = 1.3215e26
    for j in range(0,2):
        U = np.sqrt((G_value/r)*(4. - (1./A1OR) - (1./A2OR) - 2.*np.sqrt(A1OR*A2OR*(1.-e1**2)*(1.-e2**2)) \
                           *(np.cos(relinc) + comp1*comp2)))
        P[j] = U*r*c/np.abs(comp2)
        Urel[j] = U/3.14714e7
        comp2 = -comp2
    P12 = P[0] + P[1]
    U12 = 0.5*(Urel[0] + Urel[1])
    
    return P12, U12

def call_collprb(a1, a2, e1, e2, i1, i2):
    """
    Calculates the collision probability. 
    Translation from fortran to python of the code colpro.f given by D. Nesvorny.
    + addition of a probability-weighted average collision velocity
    Parameters
    ----------
    a1, a2 : float
        semi-major axis of body and comet respectively
    e1, e2 : float
        eccentricty of body and comet respectively
    i1, i2 : float
        inclination of body and comet respectively

    Returns
    -------
    Tot_CP: float
        probability of collision between body and comet per km2 per year
    AverU: float
        average collision velocity in km/s
    WeightedU: float
        probability-weighted average collision velocity in km/s
    """
    a1 = a1 * 1.496e8  # conversion from AU to km
    a2 = a2 * 1.496e8
    istepno1 = 30  # number of steps for the integration
    istepno2 = 30
    
    #IT IS CONVENIENT TO TAKE ALWAYS NO.1 TO BE THE BODY WITH LOWER ECCENTRICITY
    if e2 < e1:
        a01 = a1
        a1 = a2
        a2 = a01
        e01 = e1
        e1 = e2
        e2 = e01
        i01 = i1
        i1 = i2
        i2 = i01
        
    ra2 = a2 * (1+e2)  # aphelion of the comet
    rp2 = a2 * (1-e2)  # perihelion of the comet
    asqsqr1 = a1**2 * np.sqrt(1 - e1**2)
    asqsqr2 = a2**2 * np.sqrt(1 - e2**2)
    
    #WHEN THE INCLINATIONS ARE EXACTLY EQUAL, THE COLLISION PROBABILITY BECOMES INFINITE, THOUGH THE 
    #CORRESPONDING INTEGRAL CONVERGES. THE FOLLOWING STATEMENTS AVOID ZERO DIVIDE PROBLEMS; MOREOVER, 
    #THE STEP NUMBER ISTEPNO1 IS INCREASED BY UP TO A FACTOR 50 TO OBTAIN A BETTER ACCURACY.
    if np.abs(i2-i1) < 0.001:
        i1 = i1 + 0.001
        i2 = i2 - 0.001
    if np.abs(i2-i1) < 0.05:
        istepno1 = istepno1 * int(1./(20.*np.abs(i2-i1)))
    
    #WHEN ONE OF THE TWO ORBITS IS CIRCULAR, THE COLLISION PROBABILITY CAN BECOME INFINITE, THOUGH THE 
    #CORRESPONDING INTEGRAL CONVERGES. THE FOLLOWING STATEMENTS DEAL WITH THIS POSSLITY IN THE SAME WAY
    #AS ABOVE, ALLOWING ISTEPNO2 TO INCREASE.
    if e1 < 0.001:
        e1 = 0.001
    if e2 < 0.001:
        e2 = 0.001
    if e1 < 0.025:
        istepno2 = istepno2 * int(1./(20.*e1))
    
    #IF THE INTERSECTION CAN OCCUR AT PERIHELION OR APHELION OF BODY NO.2, THE COLLISION PROBABILITY IS 
    #INFINITE, THOUGH THE CORRESPONDING INTEGRAL CONVERGES. THE FOLLOWING STATEMENTS DEAL WITH THIS
    #POSSIBILITY IN THE SAME WAY AS ABOVE, ALLOWING ISTEPNO2 TO INCREASE.
    flag = 0
    for jj in range(1, 1001):
        angstep3 = math.pi/1000.
        truano = (jj - 0.5) * angstep3
        r1 = a1*(1. - e1**2)/(1.+ e1*np.cos(truano))
        if np.abs(ra2-r1)<(0.005*r1) or np.abs(rp2-r1)<(0.005*r1):
            if flag == 0:
                istepno2 = istepno2*20
                flag = 1
    #THE FOLLOWING STATEMENTS REDUCE IN A PROPORTIONAL WAY ISTEPNO1 AND ISTEPNO2 IF THEIR PRODUCT EXCEEDS 20000.
    if (istepno1*istepno2) >= 20000:
        redfac = np.sqrt(20000./(istepno1*istepno2))
        istepno1 = int(istepno1*redfac)
        istepno2 = int(istepno2*redfac)
        
    angstep1 = math.pi/istepno1
    angstep2 = math.pi/istepno2
    Tot_CP = 0.
    AverU = 0.
    WeightedUb = 0.  # intermediary value for the weighted velocity (before division by Tot_CP)
    WeightedU = 0.
    cosi1i2 = np.cos(i1)*np.cos(i2)
    sini1i2 = np.sin(i1)*np.sin(i2)
    
    #INTEGRAL OVER THE MUTUAL NODAL LONGITUDE, CORRESPONDING TO VARYING THE MUTUAL INCLINATION RELINC.
    for k in range(1, istepno1+1):
        cosrel = cosi1i2 + sini1i2 * np.cos((k-0.5)*angstep1)
        relinc = np.arccos(cosrel)
        c = 1./(8.*(math.pi**2)*np.sin(relinc)*asqsqr1*asqsqr2)
        P_tot = 0.
        U_tot = 0.
        U_weighted_sum = 0.
        
        #INTEGRAL OVER THE ARGUMENT OF PERIHELION OF BODY 1, I.E. OVER THE TRUE ANOMALY IN 1'S ORBIT WHERE 
        #THE MUTUAL LINES OF NODES LIES. THE PROBABILITY, COMPUTED BY S.R. CPCOMP, IS NOT ZERO ONLY WHEN ORBITAL
        #CROSSING IS GEOMETRICALLY POSSIBLE. THE AVERAGE IMPACT VELOCITY IS COMPUTED BY COUNTING HOW MANY TIMES 
        #FUNCTION CPCOMP IS CALLED, BY MEANS OF THE PARAMETER ICONT.
        icont = 0
        for kk in range(1, istepno2+1):
            truan = (kk-0.5)*angstep2
            r = a1*(1. - e1**2)/(1. + e1*np.cos(truan))
            A1OR = a1/r
            A2OR = a2/r
            x1 = ((A1OR*e1)**2 - (A1OR-1.)**2)
            x2 = ((A2OR*e2)**2 - (A2OR-1.)**2)
            if x1 <= 0 or x2 <= 0:
                truan = truan + math.pi
                r = a1*(1. - e1**2)/(1. + e1*np.cos(truan))
                x1 = ((A1OR*e1)**2 - (A1OR-1.)**2)
                x2 = ((A2OR*e2)**2 - (A2OR-1.)**2)
                if x1 > 0 and x2 > 0:
                    P12=0.
                    U12=0.
                    P12 = cp_comp(A1OR, A2OR, x1, x2, e1, e2, r, relinc, c)[0]
                    U12 = cp_comp(A1OR, A2OR, x1, x2, e1, e2, r, relinc, c)[1]
                    P_tot = P_tot + P12 * angstep2/math.pi
                    U_tot = U_tot + U12
                    U_weighted_sum = U_weighted_sum + (P12 * U12 * angstep2/math.pi)
                    icont = icont + 1
            else:
                P12=0.
                U12=0.
                P12 = cp_comp(A1OR, A2OR, x1, x2, e1, e2, r, relinc, c)[0]
                U12 = cp_comp(A1OR, A2OR, x1, x2, e1, e2, r, relinc, c)[1]
                P_tot = P_tot + P12 * angstep2/math.pi
                U_tot = U_tot + U12
                U_weighted_sum = U_weighted_sum + (P12 * U12 * angstep2/math.pi)
                icont = icont + 1
        
        if icont == 0:
            U_tot = 0.
        else:
            U_tot = U_tot / icont

        Tot_CP = Tot_CP + P_tot*angstep1/math.pi
        AverU = AverU + U_tot*angstep1/math.pi

        WeightedUb = WeightedUb + U_weighted_sum*angstep1/math.pi  # Accumulate overall weighted velocity

        # Decimal library should be used to divide 2 very small numbers 
        small_number1 = Decimal(str(WeightedUb))
        small_number2 = Decimal(str(Tot_CP))

        try:
            WeightedU = small_number1 / small_number2  # Final weighted average velocity based on total probability
        except InvalidOperation:
            WeightedU = Decimal(0)
        
    return Tot_CP, AverU, WeightedU

def _run_coll_prob(Vesta_planetesimals, asteroid_list):
    # -----------------------------------------------------------------------------
    # Calculate the collision probability with Opik method and write in a file
    # -----------------------------------------------------------------------------
    # For every Vesta-like planetesimal and for each timestep of this planetesimal lifetime, calculate  
    # the probability of impact with every comet

    # Create files that store the names of planetesimals that are already done or started (for the restart function)
    # Create the path
    pl_done_path = os.path.join(OUTPUT_DIR, "pl_done_as.txt")
    pl_start_path = os.path.join(OUTPUT_DIR, "pl_start_as.txt")
    
    # Ensure the output directory exists
    os.makedirs(OUTPUT_DIR, exist_ok=True)
    
    # Open or create the files
    open(pl_done_path, 'a+').close()
    open(pl_start_path, 'a+').close()

    body = Vesta_planetesimals
    lifetime_body = len(body.time)
    
    # -----------------------------------------------------------------------------
    # Flags for restarting the simulation where it stopped in the last run
    # -----------------------------------------------------------------------------
    # flag_done is a flag that determines if the calculations of the planetesimals are already done (entirely) or not
    # If flag_done is False, it will then check whether the planetesimal file already exists or not (flag_start True or False respectively)
    flag_done = False 
    with open(pl_done_path, 'r') as f:
        for line in f:
            if 'pl'+ str(body.idx) in line:
                flag_done = True
    
    # If flag_done is True, the calculations for this planetesimal are already done, so nothing happens 
    if flag_done is True:
        return
    
    # flag_start is a flag that determines whether the planetesimal file already exists or not (if it already exists, it must be continued where it was stopped)
    flag_start = False 
    with open(pl_start_path, 'r') as f2: # 
        for line2 in f2:
            if 'pl'+ str(body.idx) in line2:
                flag_start = True
    
    # -----------------------------------------------------------------------------
    # Determinate at what timestep the calculations should start
    # -----------------------------------------------------------------------------
    last_time = 0.
    
    # Name the file of the planetesimal
    filename_start = "pl" + str(body.idx) + ".dat"
    filepath_start = os.path.join(OUTPUT_DIR, filename_start)
    
    open(filepath_start, 'a+').close()
    
    if flag_start is True:
        # Open the file of the planetesimal
        with open(filepath_start, "r") as file_start:
            last_time = len(file_start.readlines()) # number of lines in the file
    else:
        last_time = 0.
        # Write in the file pl_start_as.txt the name of the planetesimal for which the calculations have started
        with open(pl_start_path, 'a+') as f2: 
            f2.write('pl'+ str(body.idx) + '\n')
    
    # -----------------------------------------------------------------------------
    # Loop for the probability calculations
    # -----------------------------------------------------------------------------
    for t in range(int(last_time), lifetime_body):
        proba_sum_t = 0.
        Averu_weighted_list = []
        Averu = 0.
        Averu_weighted = 0.
        Averu_min_weighted = 0.
        Averu_max_weighted = 0.
        Averu_norm_t = 0.
        Averu_median_weighted = 0.
        
        for j, asteroid in enumerate(asteroid_list):
            lifetime_asteroid = len(asteroid.time)
            if t < lifetime_asteroid:
                if body.q[t] < asteroid.qq[t] and body.qq[t] > asteroid.q[t]:  # then orbits overlap
                    proba_t = 0.
                    # proba of collision per km^2 per year
                    coll_t = call_collprb(body.a[t], asteroid.a[t], body.e[t], asteroid.e[t], body.inc[t], asteroid.inc[t])[0]
                    # collision velocity in km/s
                    Averu = call_collprb(body.a[t], asteroid.a[t], body.e[t], asteroid.e[t], body.inc[t], asteroid.inc[t])[1]
                    # collision velocity in km/s weighted by the collision probability
                    Averu_weighted = call_collprb(body.a[t], asteroid.a[t], body.e[t], asteroid.e[t], body.inc[t], asteroid.inc[t])[2] 
                    
                    # Calculation of the escape velocity of the Earth analog embryo, the gravitational focus and the radii of both the asteroid and the embryo
                    esc_vel = np.sqrt(2.*G.value*body.mass[t]/body.radius_m[t])  # in m/s because G is in m^3*kg^-1*s^-2
                    esc_vel_kms = esc_vel/1000. # in km/s
                    focus=1.0+(esc_vel_kms**2)/(float(Averu_weighted)**2)
                    radius_km_e = body.radius_m[t]/1000.  # radius of the Earth analog embryo in km
                    radius_km_c = asteroid.radius_m[t]/1000.  # radius of the asteroid in km
                
                    # Collision probability per Myr with a gravitational focus and taking into account 
                    # the planetesimal radius
                    proba_t = coll_t*((radius_km_e + radius_km_c)**2)*focus * 1.e6  # per Myr 
                    
                    # Add the weighted velocity to the list
                    Averu_weighted_list.append(float(Averu_weighted))

                else:
                    proba_t = 0.
            if t >= lifetime_asteroid:
                proba_t = 0.
            
            proba_sum_t = proba_sum_t + proba_t
            
        # Calculate the min, max, and average/median of the weighted velocities
        if Averu_weighted_list:
            Averu_min_weighted = min(Averu_weighted_list)
            Averu_max_weighted = max(Averu_weighted_list)
            Averu_norm_t = sum(Averu_weighted_list) / len(Averu_weighted_list)  # Average
            Averu_median_weighted = sorted(Averu_weighted_list)[len(Averu_weighted_list) // 2]  # Median (simplified)
        else:
            Averu_min_weighted = 0.
            Averu_max_weighted = 0.
            Averu_norm_t = 0.
            Averu_median_weighted = 0.
            
        # Write in the file of the planetesimals the results of the calculations
        with open(filepath_start, "a+") as file_start:
            # time, collision probability, mass, weighted average and median collision velocity (taken over all asteroids), minimum and maximum collision velocity when weighted by the collision probability at each orientation (taken over all asteroids)
            file_start.write(str(body.time[t]) + ' ' + str(proba_sum_t) + ' ' + str(body.mass[t]) + ' ' + str(Averu_norm_t) + ' ' + str(Averu_median_weighted) + ' ' + str(Averu_min_weighted)+ ' ' + str(Averu_max_weighted) + '\n')
        
    # -----------------------------------------------------------------------------
    # When the loop is over - Write in the file pl_done_as.txt the name of 
    # the planetesimal for which the calculations are finished 
    # -----------------------------------------------------------------------------   
    with open(pl_done_path, 'a+') as f:
        f.write('pl'+ str(body.idx) + '\n')
    
def _run_coll_wrapper(Vesta_planetesimals):
    return _run_coll_prob(Vesta_planetesimals, asteroid_list)

if __name__ == '__main__':
    # Initialize the body list
    body_list = [body(filename) for filename in glob.glob(os.path.join(BASE_PATH, "Outclement_p*.dat"))]

    # -----------------------------------------------------------------------------
    # Find all the Vesta-like bodies (bodies from inner solar system that were ejected to the asteroid belt):
    # -----------------------------------------------------------------------------
    # Create a list with the Vesta-like bodies 
    Vesta_list = []
    for body in body_list:
        # Vesta bodies on a stable orbit (e<=0.3 and i<=20°)
        if body.len_time >= 4566 and 40 <= body.idx < 2040 and body.mass[-1] < 1.0e-07 and 2 < body.a[-1] < 3 and body.e[-1] < 0.37 and np.degrees(body.inc[-1]) < 22.0:
            Vesta_list.append(body)
            
    # -----------------------------------------------------------------------------
    # Find all the asteroids in the simulation:
    # -----------------------------------------------------------------------------
    # Create a list with the asteroids 
    asteroid_list = []
    for body in body_list:
        q = body.q[0]  # q
        qq = body.qq[0] # Q
        if q > 0.7 and q < 1.5 and qq < 5.5 and body.idx >= 2040: 
            asteroid_list.append(body)

    # run coll_prob in parallel    
    #parallel = multiprocessing.Pool(5)
    parallel = ProcessingPool(2)
    items = [(Vesta_list[k]) for k in range(len(Vesta_list))]
    _ = parallel.map(_run_coll_wrapper, items)
    parallel.close()